<?php

/**
 * File: test/unit/PlayerTest.php
 */

declare(strict_types=1);

namespace Battleship\Test\Player;

use Battleship\Cli;
use Battleship\Player\HumanPlayer;
use Battleship\Ship\PatrolBoat;
use Battleship\Ship\Submarine;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    /**
     *
     * @return void
     */
    public function testConstructor() : void
    {
        $ui = $this->createStub(Cli::class);
        $patrolBoat = $this->createMock(PatrolBoat::class);
        $submarine = $this->createMock(Submarine::class);
        $player = new HumanPlayer(__METHOD__, $ui, false);
        $player->addShip($patrolBoat, $submarine);
        $this->assertEquals(HumanPlayer::class, $player::class);
    }
}
