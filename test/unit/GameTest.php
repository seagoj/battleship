<?php

/**
 * File: test/unit/Battleship.php
 *
 * Unit tests for test/unit/GameTest.php
 */

declare(strict_types=1);

namespace Battleship\Test\Unit;

use Battleship\Cli;
use Battleship\Game;
use Battleship\Position;
use Battleship\Player\ComputerPlayer;
use Battleship\Player\HumanPlayer;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    /**
     * testConstruction
     *
     * @return void
     */
    public function testConstruction() : void
    {
        $ui = $this->createMock(Cli::class);
        $ui->expects($this->once())
            ->method('clear')
            ->with();
        $player = $this->createStub(HumanPlayer::class);
        $computer = $this->createStub(ComputerPlayer::class);
        $battleship = new Game($ui, $player, $computer);
        $this->assertEquals(Game::class, $battleship::class);
    }
}
