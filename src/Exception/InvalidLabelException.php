<?php

/**
 * File: src/Exception/OverlappingShipException.php
 */

declare(strict_types=1);

namespace Battleship\Exception;

use Exception;

class InvalidLabelException extends Exception
{
}
