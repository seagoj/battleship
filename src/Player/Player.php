<?php

/**
 * File: src/Player.php
 */

declare(strict_types=1);

namespace Battleship\Player;

use Battleship\Game;
use Battleship\Ship\Battleship;
use Battleship\Ship\Carrier;
use Battleship\Ship\PatrolBoat;
use Battleship\Position;
use Battleship\Ship\Ship;
use Battleship\Ship\Submarine;
use BattleShip\Ui;

abstract class Player
{
    protected string $name;
    protected array $ships = [];
    protected int $health = 0;
    protected array $positions = [];
    protected Ui $ui;

    /**
     * Player::__construct
     *
     * Creates a player with teh given name and initializes ships
     *
     * @param string $name           Name of player
     * @param UI     $ui             Implementation of UI
     * @param bool   $generate_ships If true, populate ships array
     */
    public function __construct(string $name, UI $ui, bool $generate_ships = true)
    {
        $this->name = $name;
        $this->ui = $ui;

        if ($generate_ships) {
            $this->addShip(new PatrolBoat);
            $this->addShip(new Submarine);
            $this->addShip(new Battleship);
            $this->addShip(new Carrier);
        }
    }

    abstract public function promptForAttack(Player $opponent) : Position;

    /**
     * Return $this->positions
     *
     * @param array
     */
    public function getPositions() : array
    {
        return $this->positions;
    }

    /**
     * Return $this->name
     *
     * @param string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Player::attack
     *
     * Handles on attack on the Player
     *
     * @param Position $attack Label of the square being attacked
     *
     * @return ?Ship
     */
    public function attack(Position $attack) : ?Ship
    {
        foreach ($this->ships as $ship) {
            if ($ship->isHit($attack)) {
                $this->positions[$attack->getLabel()] = Game::HIT;
                $this->health--;
                return $ship;
            }
        }
        $this->positions[$attack->getLabel()] = Game::MISS;
        return null;
    }

    /**
     * Player::hasLost
     *
     * Calculates whether player has lost all ships
     *
     * @return bool True if player's health is 0
     */
    public function hasLost() : bool
    {
        return 0 === $this->health;
    }

    /**
     * Player::addShip
     *
     * Adds Ship to player
     *
     * @param Ship $ship
     *
     * @return void
     */
    public function addShip(Ship $ship) : void
    {
        while(true) {
            $r = random_int(0, 9);
            $c = random_int(0, 9);
            $random_position = new Position($r, $c);

            if (isset($this->positions[$random_position->getLabel()])) {
                continue;
            }

            $start = $random_position;
            $length = $ship->getLength();

            // all points $ship->length away from $start
            // randomize whether it searches vertically or horizontally first
            if (random_int(0,1)) {
                $possible_ends = [
                    new Position($r, $c+$length-1),
                    new Position($r, $c-$length+1),
                    new Position($r+$length-1, $c),
                    new Position($r-$length+1, $c),
                ];
            } else {
                $possible_ends = [
                    new Position($r+$length-1, $c),
                    new Position($r-$length+1, $c),
                    new Position($r, $c+$length-1),
                    new Position($r, $c-$length+1),
                ];
            }

            // search each possibility for a solution
            foreach ($possible_ends as $end) {
                if ($ship_positions = $this->findPositionsMaybe($start, $end)) {
                    // solution found
                    $ship->placeAt($ship_positions);
                    $this->positions = array_merge($this->positions, $ship_positions);
                    $this->health += $ship->getLength();
                    $this->ships[] = $ship;
                    return;
                }
            }
        }
    }

    /**
     * Ship::findPositionsMaybe
     *
     * @param Position $begin Position to begin search
     * @param Position $end   Final Position in search
     *
     * @return ?array Array of Positions in path from $begin to $end; null if none exist
     */
    private function findPositionsMaybe(Position $begin, Position $end) : ?array
    {
        if ($begin->getRow() === $end->getRow()) {
            $static_direction = 'getRow';
            $search_direction = 'getCol';
        } else if ($begin->getCol() === $end->getCol()) {
            $static_direction = 'getCol';
            $search_direction = 'getRow';
        } else {
            return null;
        }

        $min = min($begin->$search_direction(), $end->$search_direction());
        if ($min < 0) {
            return null;
        }

        $max = max($begin->$search_direction(), $end->$search_direction());
        if ($max > 9) {
            return null;
        }

        // check the path from $begin to $end for ships
        $static_value = $begin->$static_direction();
        $ship_positions = [];
        for ($i=$min; $i<=$max; $i++) {
            if ('getRow' === $search_direction) {
                // check horizontally
                $position_to_check = new Position($i, $static_value);
            } elseif ('getCol' === $search_direction) {
                // check vertically
                $position_to_check = new Position($static_value, $i);
            }

            // Position already occupied
            if (isset($this->positions[$position_to_check->getLabel()])) {
                return null;
            }

            $ship_positions[$position_to_check->getLabel()] = Game::SHIP;
        }

        // if we've made it this far then the path from $begin to $end is clear
        return $ship_positions;
    }
}
