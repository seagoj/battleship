<?php

/**
 * File: src/Player.php
 */

declare(strict_types=1);

namespace Battleship\Player;

use Battleship\Position;
use Throwable;

class HumanPlayer extends Player
{
    /**
     * HumanPlayer::promptForAttack
     *
     * Prompt user for next attack
     *
     * @param Postition $opponent
     *
     * @return Position Next Attack
     */
    public function promptForAttack(Player $opponent) : Position
    {
        $position = null;
        do {
            try {
                $attack = $this->ui->prompt("Which space would you like to attack?");
                $position = Position::fromLabel($attack);
            } catch(Throwable $t) {
                $this->ui->println($t->getMessage());
                $this->ui->println("Invalid position {$attack}. Please select another.");
            }
        } while (!$position);

        return $position;
    }
}
