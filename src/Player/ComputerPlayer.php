<?php

/**
  * File: src/ComputerPlayer.php
 */

declare(strict_types=1);

namespace Battleship\Player;

use Battleship\Game;
use Battleship\Position;

class ComputerPlayer extends Player
{
    /**
    * ComputerPlayer::promptForAttack
    *
    * Provides positions for computer
    *
    * @param Player $opponent
    *
    * @return Position Open Postion on opponent's board
    */
    public function promptForAttack(Player $opponent) : Position
    {
        // sleep so the computer's turn is not instantaneous
        sleep(1);
        do {
            $c = random_int(0, 9);
            $r = random_int(0, 9);
            $random_position = new Position($r, $c);
            $positions = $opponent->getPositions();
            $continue = isset($positions[$random_position->getLabel()]) && $positions[$random_position->getLabel()] === Game::MISS;
        } while($continue);

        return $random_position;
    }
}
