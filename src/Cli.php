<?php

/**
 * File: src/Cli.php
 */

declare(strict_types=1);

namespace Battleship;

class Cli implements UI
{
    /**
     * Clears cli
     *
     * @return void
     */
    public function clear() : void
    {
        system('clear');
    }

    /**
     * Print message in cli
     *
     * @return void
     */
    public function print(string $message) : void
    {
        echo "{$message}";
    }

    /**
     * Print message in cli as a line
     *
     * @return void
     */
    public function println(string $message) : void
    {
        echo "{$message}\n";
    }

    /**
     * Prompt the user to provide input
     *
     * @return string
     */
    public function prompt(string $message) : string
    {
        $this->println($message);
        $handle = fopen('php://stdin', 'r');

        // get response and trim whitespace
        return trim(fgets($handle));
    }
}
