<?php

/**
 * File: src/Battleship.php
 */

declare(strict_types=1);

namespace Battleship;

use Battleship\Player\Player;
use Battleship\Player\ComputerPlayer;
use Battleship\Player\HumanPlayer;
use Throwable;

class Game
{
    // symbols to display on board
    const HIT = 'X ';
    const MISS = 'o ';
    const UNKNOWN = '- ';
    const SHIP = '# ';

    /**
     * Game::__construct
     *
     * Initializes the game by adding 2 players
     */
    public function __construct(UI $ui, HumanPlayer $player = null, ComputerPlayer $computer = null)
    {
        $this->ui = $ui;
        $this->ui->clear();
        $this->players[] = $player ?? new HumanPlayer("Player", $ui);
        $this->players[] = $computer ?? new ComputerPlayer("Computer", $ui);
    }

    /**
     * Game::run
     *
     * Main loop for the game
     *
     * Creates loop for each round of play; only exits when there's a winner
     *
     * @return void
     */
    public function run() : void
    {
        $has_won = false;
        while(!$has_won) {
            $has_won = $this->handleTurn($this->players[0], $this->players[1]);

            // short circuit if player 1 $has_won
            if ($has_won) {
                break;
            }

            $has_won = $this->handleTurn($this->players[1], $this->players[0]);
        }

        $this->scoreboard();
    }

    /**
     * Game::handleTurn
     *
     * Begins the player's turn and returns state of game
     *
     * @param Player $player
     * @param Player $opponent
     *
     * @return bool True if either player has won
     */
    private function handleTurn(Player $player, Player $opponent) : bool
    {
        while (true) {
            try {
                // display initial view for attack
                $this->ui->clear();
                $this->ui->println("{$player->getName()}:");
                $this->displayBoard($opponent);
                $attack = $player->promptForAttack($opponent);

                // display result of attack
                $this->ui->clear();
                if ($attacked_ship = $opponent->attack($attack)) {
                    $this->ui->println("{$player->getName()}: {$attack->getLabel()} HIT");
                    if ($attacked_ship->isSunk()) {
                        $this->ui->println("You sank my {$attacked_ship->getName()}!");
                    }
                } else {
                    $this->ui->println("{$player->getName()}: {$attack->getLabel()} MISS");
                }

                if (!$opponent->hasLost()) {
                    $this->displayBoard($opponent);
                    sleep(2);
                    $this->ui->clear();

                    return false;
                }

                $this->ui->println("{$player->getName()} wins!");

                return true;
            } catch (Throwable $t) {
                $this->ui->println($t->getMessage());
            }
        }
    }

    /**
     * Game::displayBoard
     *
     * Displays current player's displayBoard
     *
     * TODO make this more readible
     *
     * @param Player $player     Player to display
     * @param bool   $show_ships Conditionally show the player's unhit ships on the displayBoard
     *
     * @return void
     */
    public function displayBoard(Player $player, bool $show_ships = false) : void
    {
        $positions = $player->getPositions();
        for ($c=0; $c<10; $c++) {
            if (0 == $c) {
                $this->ui->println("  1 2 3 4 5 6 7 8 9 10");
            }
            $label = Position::ROW_LABEL_MAP[$c];
            $this->ui->print("$label ");
            for ($r=0; $r<10; $r++) {
                $position = new Position($c, $r);

                // empty space
                if (!isset($positions[$position->getLabel()])) {
                    $this->ui->print(Game::UNKNOWN);
                    continue;
                }

                // miss, hit or ship if $show_ships = true
                $symbol = $positions[$position->getLabel()];
                if ($symbol === Game::SHIP) {
                    $this->ui->print($show_ships ? Game::SHIP : Game::UNKNOWN);
                } else {
                    $this->ui->print($symbol);
                }
            }
            $this->ui->println("");
        }
    }

    /**
     * Game::scoreboard
     *
     * Displays both boards with ships showing
     *
     * @return void
     */
    private function scoreboard() : void
    {
        foreach ($this->players as $player) {
            $this->ui->println("{$player->getName()}:");
            $this->displayBoard($player, true);
        }
    }
}
