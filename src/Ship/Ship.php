<?php

/**
 * File: src/Ship/Ship.php
 */

declare(strict_types=1);

namespace Battleship\Ship;

use Battleship\Game;
use Battleship\Position;

abstract class Ship
{
    protected int $length = 0;
    protected string $name;
    protected array $positions = [];

    /**
     * Ship::getLength
     *
     * @return int
     */
    public function getLength() : int
    {
        return $this->length;
    }

    /**
     * Ship::getName
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Ship::isHit
     *
     * Checks if $attack is a hit on the ship
     *
     * @return bool
     */
    public function isHit(Position $attack) : bool
    {
        if (isset($this->positions[$attack->getLabel()]) &&
            Game::SHIP === $this->positions[$attack->getLabel()]
        ) {
            $this->addPosition($attack, Game::HIT);
            return true;
        }
        $this->addPosition($attack, Game::MISS);

        return false;
    }

    /**
     * Ship::isSunk
     *
     * Checks if ship is sunk
     *
     * @return bool
     */
    public function isSunk() : bool
    {
        return !in_array(Game::SHIP, $this->positions);
    }

    /**
     * Ship::placeAt
     *
     * Sets ship at $positions in game
     *
     * @param Position[] $positions Array of Positions to place Ship
     *
     * @return void
     */
    public function placeAt(array $positions) : void
    {
        $this->positions = $positions;
    }

    /**
     * Ship::addPosition
     *
     * Adds Position to Ship
     *
     * @param Position $position Position to add to Ship
     * @param String   $symbol   Value of ship at Position
     */
    private function addPosition(Position $position, string $symbol) : void
    {
        $this->positions[$position->getLabel()] = $symbol;
    }
}
