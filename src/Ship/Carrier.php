<?php

/**
 * File: src/Ship/Carrier.php
 */

declare(strict_types=1);

namespace Battleship\Ship;

class Carrier extends Ship
{
    protected int $length = 5;
    protected string $name = 'Carrier';
}
