<?php

/**
 * File: src/Ship/PatrolBoat.php
 */

declare(strict_types=1);

namespace Battleship\Ship;

class PatrolBoat extends Ship
{
    protected int $length = 2;
    protected string $name = 'Patrol Boat';
}
