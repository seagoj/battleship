<?php

/**
 * File: src/Ship/Battleship.php
 */

declare(strict_types=1);

namespace Battleship\Ship;

class Battleship extends Ship
{
    protected int $length = 4;
    protected string $name = 'battleship';
}
