<?php

/**
 * File: src/Ship/Submarine.php
 */

declare(strict_types=1);

namespace Battleship\Ship;

class Submarine extends Ship
{
    protected int $length = 3;
    protected string $name = 'Submarine';
}
