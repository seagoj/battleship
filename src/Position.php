<?php

/**
 * File: src/Position.php
 */

declare(strict_types=1);

namespace Battleship;

use Battleship\Exception\InvalidLabelException;
use Throwable;

class Position
{
    public int $col;
    public int $row;

    const ROW_LABEL_MAP = [
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J'
    ];

    const LABEL_ROW_MAP = [
        'A' => 0,
        'B' => 1,
        'C' => 2,
        'D' => 3,
        'E' => 4,
        'F' => 5,
        'G' => 6,
        'H' => 7,
        'I' => 8,
        'J' => 9
    ];

    public function __construct(int $row, int $col)
    {
        $this->row = $row;
        $this->col = $col;
    }

    /**
     * label
     *
     * @return
     */
    public function getLabel() : string
    {
        return self::ROW_LABEL_MAP[$this->row] . ($this->col + 1);
    }

    public function getCol() : int
    {
        return $this->col;
    }

    public function getRow() : int
    {
        return $this->row;
    }

    public static function fromLabel(string $label) : self
    {
        try {
            $label = ucfirst($label);

            $row = self::LABEL_ROW_MAP[substr($label, 0, 1)];
            $col = (int) substr($label, 1) - 1;

            if (!isset(self::ROW_LABEL_MAP[$row]) || $col < 0 || $col > 9) {
                throw new InvalidLabelException;
            }
            return new self($row, $col);
        } catch (Throwable $e) {
            throw new InvalidLabelException;
        }
    }
}
