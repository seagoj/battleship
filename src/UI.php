<?php

/**
 * File: src/UI.php
 */

declare(strict_types=1);

namespace Battleship;

interface UI
{
    public function clear() : void;
    public function print(string $message) : void;
    public function println(string $message) : void;
    public function prompt(string $message) : string;
}
