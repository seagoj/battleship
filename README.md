# Battleship

This is a commandline implemention of the board game Battleship.

Written againd PHP 8.1.11, but I believe it would be backward compatible to 7.2 at least.

## Installation

### Dependencies

`composer install`

## Usage

`bin/battleship`

## Tests

`composer test`
